#!/usr/bin/php
<?php

$str = <<<EOF
/*

    This code belongs to the "EyeSkills Framework". It is deigned to assist
    in the development of software for the human/animal visual system. 

    Copyright(C) 2018 Dr. Thomas Benjamin Senior, Michael Zoller.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.If not, see<https://www.gnu.org/licenses/>.
*/

EOF;

echo "hello\n";

function rsearch($folder, $pattern) {
    $dir = new RecursiveDirectoryIterator($folder);
    $ite = new RecursiveIteratorIterator($dir);
    $files = new RegexIterator($ite, $pattern, RegexIterator::GET_MATCH);
    $fileList = array();
    foreach($files as $file) {
        $fileList = array_merge($fileList, $file);
	echo ("Found file \n");
	print_r($file);
    }
    return $fileList;
}

//$found_files = rsearch("/Users/bensenior/WORK/QuickLinks/EyeSkillsUnity/Assets/EyeSkills","/.*.cs/");
//Do NOT want to match something.cs.meta! Otherwise we write into the something.cs file twice
$found_files = rsearch("/Users/bensenior/WORK/WORKSPACES-Programming/PrototypeFund/eyeskills/Unity/PrototypeFund/Assets/EyeSkills","/.*.cs$/");

foreach ($found_files as $key => $file){
	$contents = file_get_contents($file);
	file_put_contents($file,$str."\n".$contents);
	echo "Wrote out new contents to ".$file."\n";
}


//print_r($found_files);
?>
