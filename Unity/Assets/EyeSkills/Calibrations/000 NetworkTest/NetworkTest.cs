/*
    This code belongs to the "EyeSkills Framework". It is designed to assist
    in the development of software for the human/animal visual system.

    Copyright(C) 2018 Dr. Thomas Benjamin Senior, Michael Zöller.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using EyeSkills;

namespace EyeSkills.Calibrations
{
    public class NetworkTest : MonoBehaviour
    {
        [System.Serializable]
        public class NetworkTestConfig : ConfigBase
        {
            public int counter = 0;
            public string someString = "kdjfkdjf";
            public Vector3 someVector = Vector3.one;
        }

        public NetworkTestConfig config;

        public GameObject target;
        public UnityEngine.UI.Text uiText;


        float pos = 0f;

        void Start()
        {
            NetworkManager.instance.RegisterScene("Network Test", "Network debugging calibration scene.");
            NetworkManager.instance.RegisterFloat("pos", 0f, 1f, .1f, "Position",
                "The Position of the (left eye only) cube.");
            NetworkManager.instance.RegisterButton("quit", "Quit the scene",
                "This button will exit the scene and return to the main menu.");

            NetworkManager.instance.Report("network start", new
            {
                a = "blah",
                b = Random.Range(-1000, 1000),
                c = Random.Range(-1000, 1000)
            });


            Debug.Log("config.counter is " + config.counter);
        }

        private void ChangePos(float newpos)
        {
            Vector3 v = target.transform.position;
            v.y = newpos;
            target.transform.position = v;
        }

        private void Update()
        {
          uiText.text = config.counter.ToString();
          config.counter++;


            if (Input.GetKeyDown(KeyCode.Return)) {
              config.counter++;


              //config.Sync();
            }

            if (pos < 1 && Input.GetKey(KeyCode.UpArrow))
            {
                pos += Time.deltaTime;
                ChangePos(pos);
                NetworkManager.instance.SetFloat("pos", pos);
            }

            if (pos > 0 && Input.GetKey(KeyCode.DownArrow))
            {
                pos -= Time.deltaTime;
                ChangePos(pos);
                NetworkManager.instance.SetFloat("pos", pos);
            }

            if (NetworkManager.instance.FloatChanged("pos"))
            {
                pos = NetworkManager.instance.GetFloat("pos");
                ChangePos(pos);
            }

            if (Input.GetKey(KeyCode.Escape) || NetworkManager.instance.GetButton("quit"))
            {
                NetworkManager.instance.Report("network end", new
                {
                    someString = "sfkdkfjdkfjdkj",
                    pos,
                    time = Time.timeSinceLevelLoad
                });

                SceneManager.LoadScene("Menu");
            }
        }
    }
}
