/*
    This code belongs to the "EyeSkills Framework". It is designed to assist
    in the development of software for the human/animal visual system.

    Copyright(C) 2018 Dr. Thomas Benjamin Senior, Michael Zöller.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EyeSkills
{
    public class AlternatorModel : MonoBehaviour
    {
        private float freq = 0f;

        public GameObject rightObj, leftObj;

        public void ShowRight(bool show)
        {
            rightObj.SetActive(show);
        }

        public void ShowLeft(bool show)
        {
            leftObj.SetActive(show);
        }

        public float AlterFrequency(float _freq)
        {
            if ((freq + _freq > -1f) && (freq + _freq <= 30f))
            {
                freq += _freq;
            }

            return freq;
        }

        public void StartSwitchingFixationObjects()
        {
            StartCoroutine(SwitchFixationObjects());
        }

        IEnumerator SwitchFixationObjects()
        {
            bool s = false;
            while (true)
            {
                if (freq > 0)
                {
                    ShowRight(s);
                    ShowLeft(!s);
                    s = !s;
                }

                float secondsToWait = (freq == 0) ? 0 : 1 / freq;
                yield return new WaitForSeconds(secondsToWait);
            }
        }
    }
}