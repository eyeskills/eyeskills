/*
    This code belongs to the "EyeSkills Framework". It is designed to assist
    in the development of software for the human/animal visual system. 

    Copyright(C) 2018 Dr. Thomas Benjamin Senior, Michael Zöller.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

 using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EyeSkills;

namespace EyeSkills.Calibrations
{
    /// <summary>
    /// Alternating fusion controller.  
    /// Short left/right alters rate at which the objects switch eye.
    /// Short up/down places them in conflict or brings them out of conflict (vertically)
    /// 
    /// FUTURE:
    /// Long up/down takes misalignment into account (useful for non alternatingStrabismics)
    /// Long left/right might then apply/remove gradually migrating the strabismic eye to the center
    /// </summary>
    public class AlternatingFusionController : MonoBehaviour
    {
        public ConflictZoneModel conflictBackground;
        public AlternatorModel alternator;

        private AudioManager audioManager;

        private float freqInc = 1f;

        void Start()
        {
            NetworkManager.instance.RegisterScene("Alternating Fusion", "Can the participant switch eyes fast enough to start fusing?");
            NetworkManager.instance.RegisterButton("instructions", "Instructions", "Have the scene instructions read to you");
            NetworkManager.instance.RegisterButton("incFrequency", "Increase Frequency", "Increase object frequency");
            NetworkManager.instance.RegisterButton("decFrequency", "Decrease Frequency", "Decrease object frequency");
            NetworkManager.instance.RegisterButton("conflict", "In conflict", "Place the objects in conflict (occupying the same perceived space))");
            NetworkManager.instance.RegisterButton("noConflict", "No conflict", "Remove conflict (objects occupy different percieved spaces)");

            alternator.ShowRight(true);
            alternator.ShowLeft(true);
            conflict(false);

            audioManager = AudioManager.instance;
            sayString("ready");
            alternator.StartSwitchingFixationObjects();
        }

        private void sayString(string text)
        {
            Debug.Log("Want to say " + text);
            audioManager.Say(text);
        }

        private void sayFrequency(float freq)
        {
            //string fileKey = (1/freq).ToString().Substring(0,3) + "Hertz";
            string f = freq.ToString();
            if (f.Length > 4)
            {
                f = f.Substring(0, 4);
            }

            string fileKey = f + "Hertz";
            Debug.Log("Looking for file key " + fileKey);
            sayString(fileKey);
        }

        bool conflict(bool inConflict)
        {
            if (inConflict)
            {
                conflictBackground.IntoConflict();
                conflictBackground.Show(true);
            }
            else
            {
                conflictBackground.Show(false);
                conflictBackground.OutOfConflict();
            }

            Debug.Log("Conflict Status " + inConflict);
            return inConflict;
        }

        void sayConflict(bool inConflict)
        {
            if (inConflict)
            {
                sayString("inConflict");
            }
            else
            {
                sayString("notInConflict");
            }
        }

        void Update()
        {
            if (EyeSkillsInput.instance.GetShortButtonPress("EyeSkills Left") || (NetworkManager.instance.GetButton("decFrequency")))
            {
                Debug.Log("Decreasing Freq - short button press left");
                sayFrequency((int) alternator.AlterFrequency(-freqInc));
            }
            else if (EyeSkillsInput.instance.GetShortButtonPress("EyeSkills Right") || (NetworkManager.instance.GetButton("incFrequency")))
            {
                Debug.Log("Increasing Freq - short button press right");
                sayFrequency((int) alternator.AlterFrequency(+freqInc));
            }
            else if (EyeSkillsInput.instance.GetLongButtonPress("EyeSkills Up") || (NetworkManager.instance.GetButton("conflict")))
            {
                Debug.Log("Conflict");
                sayConflict(conflict(true));
            }
            else if (NetworkManager.instance.GetButton("instructions"))
            {
                sayString("alternatingFusionInstructions");
            }
            else if (EyeSkillsInput.instance.GetLongButtonPress("EyeSkills Down") || (NetworkManager.instance.GetButton("noConflict")))
            {
                Debug.Log("Nonconflict");
                sayConflict(conflict(false));
            }
        }
    }
}