/*
    This code belongs to the "EyeSkills Framework". It is designed to assist
    in the development of software for the human/animal visual system.

    Copyright(C) 2018 Dr. Thomas Benjamin Senior, Michael Zöller.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections;
using UnityEngine; 
using UnityEngine.SceneManagement;

namespace EyeSkills.Calibrations
{
    /// <summary>
    /// Detect eye rotation by having the user align two images. There is a model in here for tracking eye misalignment via the head. What we do with that data, or how we present the options,
    /// are to do with a controller, so we can and should split the two parts.
    /// </summary>
    public class MisalignmentMeasurementController : MonoBehaviour
    {
        protected StereoTargetEyeMask eye = StereoTargetEyeMask.None;
        private EyeSkillsInput esInput;
        protected AudioManager audioManager;
        protected bool rightEyeIsStrabismic = false;

        public float secondsOfStillnessForSelect = 10f;
        /// <summary>
        /// Our headset stillness detection controller
        /// </summary>
        public EyeSkillsVRHeadsetSelectByStillness stillness;

        /// <summary>
        /// The selection indicator.
        /// </summary>
        public SelectionIndicatorScaler indicator;

        private EyeSkillsCameraRig cameraRig;

        private bool practitionerMode = false;

        public void Start()
        {
            cameraRig = FindObjectOfType<EyeSkillsCameraRig>();
            esInput = EyeSkillsInput.instance;

            audioManager = AudioManager.instance;
            //audioManager.Say("ChooseStrabismicEye");

            if( cameraRig.config.leftEyeIsStrabismic )
            {
                audioManager.Say("LeftStrabismic");
                unlockLeftEye();
            } else
            {
                audioManager.Say("RightStrabismic");
                unlockRightEye();
            }

            practitionerMode = (PlayerPrefs.GetString("EyeSkills.practitionerMode") == "1") ? true : false;

            NetworkManager.instance.RegisterScene("Detect Eye Misalignment", "To what degree are the participants eyes misaligned?");
            NetworkManager.instance.RegisterButton("unlockRight", "Unlock right eye", "Right eye is strabismic");
            NetworkManager.instance.RegisterButton("unlockLeft", "Unlock left eye", "Left eye is strabismic");
            NetworkManager.instance.RegisterButton("save", "Save misalignment", "Save misalignment angle");
            NetworkManager.instance.RegisterFloat("degree", -45f, 45f, 1f, "Misalignment", "Angle between the eyes.");

            StartCoroutine(trackAndReportEyeMisalignment());
        }

        protected void unlockRightEye()
        {
            eye = StereoTargetEyeMask.Right;

            cameraRig.SetRightEyeRotationAndPosition();

            cameraRig.config.leftEyeIsStrabismic = false;
            cameraRig.config.rightEyeIsStrabismic = true;
            cameraRig.config.Sync();

            rightEyeIsStrabismic = true;

            // relock left eye
            cameraRig.StraightenLeftEye();
            cameraRig.SetLeftEyePositionOnly();
        }

        protected void unlockLeftEye()
        {
            eye = StereoTargetEyeMask.Left;

            cameraRig.SetLeftEyeRotationAndPosition();

            cameraRig.config.leftEyeIsStrabismic = true;
            cameraRig.config.rightEyeIsStrabismic = false;
            cameraRig.config.Sync();

            rightEyeIsStrabismic = false;

            // relock right eye
            cameraRig.StraightenRightEye();
            cameraRig.SetRightEyePositionOnly();
        }

        /// <summary>
        /// Reports the misalignment angle to the web interface.
        /// </summary>
        /// <returns>The and log misalignment angle.</returns>
        IEnumerator trackAndReportEyeMisalignment()
        {
            //Give the system time to settle before reporting. null references crash the coroutine.
            yield return new WaitForSeconds(2f);

            while (true)
            {
                if (eye != StereoTargetEyeMask.None)
                    NetworkManager.instance.SetFloat("degree", cameraRig.HorizontalEyeAngleHumanReadable(eye));

                yield return new WaitForSeconds(.5f);
            }
        }

        private void StoreAndQuit(){
            Vector3 euler = cameraRig.GetEyeAngle(eye);
            Debug.Log("Eye angle (euler) is " + euler + " degrees.");

            if (eye == StereoTargetEyeMask.Left)
                cameraRig.config.leftEyeMisalignmentAngle = euler;
            else
                cameraRig.config.rightEyeMisalignmentAngle = euler;

            //cameraRig.config.Sync();
            SceneManager.LoadScene("Menu");
        }

        public void Update()
        {

            if (!practitionerMode)
            {   //Now lets check to see if the headset if being held still
                float still = stillness.getTimeStill();
                Debug.Log("Time still " + still);
                if (still > secondsOfStillnessForSelect)
                {
                    //Selected!
                    indicator.Reset();
                    StoreAndQuit();
                }
                else
                {
                    //redraw the indicator and play a rising tone?!?
                    float percentage = (still / secondsOfStillnessForSelect);
                    Debug.Log("Percentage still " + percentage);
                    indicator.SetIndicatorPercentage(percentage);
                }
            }


            if (Input.GetButton("EyeSkills Confirm") || NetworkManager.instance.GetButton("save"))
            {
                StoreAndQuit();
            }
            else if (esInput.GetLongButtonPress("EyeSkills Left") || NetworkManager.instance.GetButton("unlockLeft"))
            {
                audioManager.Say("LeftStrabismic");
                unlockLeftEye();
            }
            else if (esInput.GetLongButtonPress("EyeSkills Right") || NetworkManager.instance.GetButton("unlockRight"))
            {
                audioManager.Say("RightStrabismic");
                unlockRightEye();
            }
        }
    }
}
