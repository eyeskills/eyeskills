/*
    This code belongs to the "EyeSkills Framework". It is designed to assist
    in the development of software for the human/animal visual system. 

    Copyright(C) 2018 Dr. Thomas Benjamin Senior, Michael Zöller.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace EyeSkills.Calibrations
{
    /// <summary>
    /// Depth perception calibration controller.
    /// </summary>
    public class DepthPerceptionCalibrationController : MonoBehaviour
    {
        [System.Serializable]
        public class DepthPerceptionConfig : ConfigBase
        {
            public float depthPerceptionAccuracy;
        }

        public DepthPerceptionConfig config;

        /// <summary>
        /// The number of depth perception trials to run.
        /// </summary>
        public int numberOfDepthPerceptionTrials = 6;

        /// <summary>
        /// The number of correct user choices.
        /// </summary>
        private float numberOfCorrectUserChoices = 0;

        /// <summary>
        /// The milliseconds to present depth perception objects.
        /// </summary>
        public float secondsToPresentDepthPerceptionObjects = 2.0f;

        public GameObject depthChoiceUI;
        public GameObject depthObjects;
        public GameObject nearObject;
        public GameObject farObject;

        //We flip the quads from left to right depending on whether we want the left side quad
        //to appear far away or near.
        public Vector3 nearOnLeft = new Vector3(-0.71f, 0, 2.25f);
        public Vector3 farOnRight = new Vector3(3f, 0, 9.31f);

        public Vector3 nearOnRight = new Vector3(0.71f, 0, 2.25f);
        public Vector3 farOnLeft = new Vector3(-3f, 0, 9.31f);

        private float choicesMade = 0;

        private bool leftIsNearer = false;

        private IEnumerator coroutine;

        /// <summary>
        /// Randomises the side which is near on left.
        /// </summary>
        /// <returns><c>true</c>, if side which is near on left was randomised, <c>false</c> otherwise.</returns>
        private bool randomiseSideWhichIsNearOnLeft()
        {
            //Randomly place the near object on the left or right

            if (Random.value < 0.5f)
            {
                // Left Near
                nearObject.transform.position = nearOnLeft;
                farObject.transform.position = farOnRight;
                return true;
            }
            else
            {
                // Right Near
                nearObject.transform.position = nearOnRight;
                farObject.transform.position = farOnLeft;
                return false;
            }
        }

        void Start()
        {
            depthChoiceUI.SetActive(false);
            depthObjects.SetActive(false);
            coroutine = OfferChoices();
            StartCoroutine(coroutine);
        }

        private IEnumerator OfferChoices()
        {
            for (int i = 1; i <= numberOfDepthPerceptionTrials; i++)
            {
                // Randomise which side is closest
                leftIsNearer = randomiseSideWhichIsNearOnLeft();
                // Display our objects;
                depthChoiceUI.SetActive(false);
                depthObjects.SetActive(true);

                // Give them a period of time to observe this
                yield return new WaitForSeconds(secondsToPresentDepthPerceptionObjects);

                // Now hide those objects and display the selection UI.
                depthChoiceUI.SetActive(true);
                depthObjects.SetActive(false);

                // Give them a period of time to make a choice. 
                // Left was closer, right was closer, or don't know.
                // yield return new WaitForSeconds(maximumSecondsToAllowUserToMakeSelection);
                // int selection = buttonController.waitForSelection();
                int choice = 0;

                while (choice == 0)
                {
                    if (Input.GetButtonDown("EyeSkills Left"))
                        choice = -1;
                    else if (Input.GetButtonDown("EyeSkills Right"))
                        choice = 1;
                    else if (Input.GetButtonDown("EyeSkills Down"))
                        choice = -1;

                    yield return 0;
                }

                choicesMade += 1f;
                if (leftIsNearer && choice == -1 || !leftIsNearer && choice == 1)
                    numberOfCorrectUserChoices += 1f;
            }

            // store calibration and return to the menu
            config.depthPerceptionAccuracy = numberOfCorrectUserChoices / choicesMade;
            SceneManager.LoadScene("Menu");
        }
    }
}