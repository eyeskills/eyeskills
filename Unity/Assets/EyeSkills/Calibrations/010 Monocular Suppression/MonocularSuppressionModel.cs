/*
    This code belongs to the "EyeSkills Framework". It is deigned to assist
    in the development of software for the human/animal visual system. 

    Copyright(C) 2018 Dr. Thomas Benjamin Senior, Michael Zöller.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.If not, see<https://www.gnu.org/licenses/>.
*/

using UnityEngine;

namespace EyeSkills.Calibrations
{
    /// <summary>
    /// Monocular suppression controller.
    /// Short Up/Down alters luminosity.
    /// Long Up/Down alters distance from camera.
    /// Long Left/Right alters currently active eye.
    /// </summary>
    public class MonocularSuppressionModel : MonoBehaviour
    {
        private EyeSkillsInput esInput;

        public MultiFixationSprite rightOccluder, leftOccluder;
        private MultiFixationSprite currentOccluder;

        public MultiSprite PickRandomOccluder()
        {
            return currentOccluder.PickRandom();
        }

        public void SwitchToRightEye()
        {
            leftOccluder.Hide();
            rightOccluder.Show();
            currentOccluder = rightOccluder;
        }

        public void SwitchToLeftEye()
        {
            rightOccluder.Hide();
            leftOccluder.Show();
            currentOccluder = leftOccluder;
        }

        public MultiFixationSprite GetCurrentOccluder()
        {
            return currentOccluder;
        }

        public void LogState()
        {
            Debug.Log("Current fixation object is " + currentOccluder.GetCurrentKey() + " with luminosity " + currentOccluder.GetCurrentLuminosity() + " at distance " + currentOccluder.GetCurrentDistance());
        }
    }
}