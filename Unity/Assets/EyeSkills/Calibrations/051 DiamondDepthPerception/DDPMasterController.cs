/*
    This code belongs to the "EyeSkills Framework". It is designed to assist
    in the development of software for the human/animal visual system. 

    Copyright(C) 2018 Dr. Thomas Benjamin Senior, Michael Zöller.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EyeSkills.Calibrations;

namespace EyeSkills
{
    /// <summary>
    /// DDP Master controller. This might be where we, eventually, also include some expert system logic/workflow to even automate in scene processes
    /// We would have to rewrite the controllers to use onEnable and onDisable though, or add hooks to rerun start when they are called.
    /// </summary>
    public class DDPMasterController : MonoBehaviour
    {
        private FakeDepthController depthController;

        private void InitDepth()
        {
            gameObject.SetActive(true);
            depthController = gameObject.GetComponentInChildren<FakeDepthController>();
            depthController.enabled = true;
            depthController.Start();
        }

        void Start()
        {
            InitDepth();
        }
    }
}