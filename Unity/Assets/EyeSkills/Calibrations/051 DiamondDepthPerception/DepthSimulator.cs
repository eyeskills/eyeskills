/*
    This code belongs to the "EyeSkills Framework". It is designed to assist
    in the development of software for the human/animal visual system. 

    Copyright(C) 2018 Dr. Thomas Benjamin Senior, Michael Zöller.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EyeSkills;

namespace EyeSkills
{
    public class DepthSimulator : MonoBehaviour, IHorizontalFakeDepthControl
    {
        public GameObject part1, part2;
        private float horizontalSeparation, previousSeparation;
        Vector3 centerPoint;

        //float rate = 0.2f;
        //float step = 0.01f;

        void Start()
        {
            centerPoint = part1.transform.position;
        }

        void redrawPositions(float _horizontalSeparation)
        {
            Mathf.Clamp(_horizontalSeparation, 0, 40);

            if (!Mathf.Approximately(_horizontalSeparation, previousSeparation))
            {
                part1.transform.position = new Vector3(centerPoint.x + _horizontalSeparation, centerPoint.y, centerPoint.z);
                part2.transform.position = new Vector3(centerPoint.x - _horizontalSeparation, centerPoint.y, centerPoint.z);
                previousSeparation = _horizontalSeparation;
            }
        }

        public void resetHorizontalSeparation()
        {
            redrawPositions(0f);
        }

        public void setHorizontalSeparation(float separation)
        {
            redrawPositions(separation);
        }

        public void smoothlyIncreaseHorizontalSeparation(float rate)
        {
            horizontalSeparation += Time.deltaTime * rate;
            redrawPositions(horizontalSeparation);
        }

        public void smoothlyDecreaseHorizontalSeparation(float rate)
        {
            horizontalSeparation -= Time.deltaTime * rate;
            redrawPositions(horizontalSeparation);
        }

        public void stepIncreaseHorizontalSeparation(float step)
        {
            horizontalSeparation += step;
            redrawPositions(horizontalSeparation);
        }

        public void stepDecreaseHorizontalSeparation(float step)
        {
            horizontalSeparation -= step;
            redrawPositions(horizontalSeparation);
        }
    }
}