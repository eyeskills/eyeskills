/*
    This code belongs to the "EyeSkills Framework". It is designed to assist
    in the development of software for the human/animal visual system. 

    Copyright(C) 2018 Dr. Thomas Benjamin Senior, Michael Zöller.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

 using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AndroidAudioVolume : MonoBehaviour {

    public int forceVolume = -1;

    const int streammusic = 3;
    const int flagshowui = 0;

//UNITY_ANDROID
#if FALSE
    private static int GetDeviceMaxVolume()
    {
        return deviceAudio.Call<int>("getStreamMaxVolume", streammusic);
    }

    private static int GetDeviceVolume()
    {
        return deviceAudio.Call<int>("getStreamVolume", streammusic);
    }

    private static void SetDeviceVolume(int value)
    {
        deviceAudio.Call("setStreamVolume", streammusic, value, flagshowui);
    }

    private static AndroidJavaObject audioManager;

    private static AndroidJavaObject deviceAudio
    {
        get
        {
            if (audioManager == null)
            {
                AndroidJavaClass up = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
                AndroidJavaObject context = up.GetStatic<AndroidJavaObject>("currentActivity");

                string audioName = context.GetStatic<string>("AUDIO_SERVICE");

                audioManager = context.Call<AndroidJavaObject>("getSystemService", audioName);
            }
            return audioManager;
        }
    }

    private void Start()
    {
        return;

        if (Application.platform != RuntimePlatform.Android)
            return;

        Debug.Log("Device max audio volume: " + GetDeviceMaxVolume());
        Debug.Log("Device current audio volume: " + GetDeviceMaxVolume());

        if (forceVolume > -1)
            SetDeviceVolume(forceVolume);*/
    }
#endif

}
