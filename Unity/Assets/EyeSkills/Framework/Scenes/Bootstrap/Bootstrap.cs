/*
    This code belongs to the "EyeSkills Framework". It is designed to assist
    in the development of software for the human/animal visual system.

    Copyright(C) 2018 Dr. Thomas Benjamin Senior, Michael Zöller.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

 using UnityEngine;
using UnityEngine.SceneManagement;

namespace EyeSkills
{
    public class Bootstrap : MonoBehaviour
    {
        private void Start()
        {
            PlayerPrefs.SetInt("EyeSkills.MenuUnlocked", 0);

            if (SceneManager.GetActiveScene().name != "Bootstrap")
            {
                Destroy(gameObject);
                return;
            }

            if( PlayerPrefs.GetString("EyeSkills.lockExperience") != "")
                SceneManager.LoadScene(PlayerPrefs.GetString("EyeSkills.lockExperience"));
            else
                SceneManager.LoadScene("Menu");
        }
    }
}
