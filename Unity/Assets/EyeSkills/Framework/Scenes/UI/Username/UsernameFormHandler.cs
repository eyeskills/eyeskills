/*
    This code belongs to the "EyeSkills Framework". It is designed to assist
    in the development of software for the human/animal visual system.

    Copyright(C) 2018 Dr. Thomas Benjamin Senior, Michael Zöller.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

using PowerUI;
using UnityEngine;
using UnityEngine.SceneManagement;

public static class UsernameFormHandler
{
    public static void OnSubmit(FormEvent form)
    {
        form.preventDefault();

        PlayerPrefs.SetString("EyeSkills.Name", form["name"]);
        PlayerPrefs.SetString("EyeSkills.Practitioner", form["practitioner"]);
        PlayerPrefs.SetString("EyeSkills.lockExperience", form["lock"]);
        PlayerPrefs.SetString("EyeSkills.assetStyle", form["asset"]);
        PlayerPrefs.SetString("EyeSkills.remoteServerUrl", form["remoteServerUrl"]);
        if (form["practitionerMode"] == ""){
            PlayerPrefs.SetString("EyeSkills.practitionerMode", "0");
        } else {
            PlayerPrefs.SetString("EyeSkills.practitionerMode", "1");
        }


        // force save now in case editor/player crashes
        PlayerPrefs.Save();
        if (form["remoteServerUrl"] != "")
        {
            EyeSkills.NetworkManager.instance.server = form["remoteServerUrl"];
        }
        EyeSkills.NetworkManager.instance.Reconnect();

        SceneManager.LoadScene("Menu");
    }
}
