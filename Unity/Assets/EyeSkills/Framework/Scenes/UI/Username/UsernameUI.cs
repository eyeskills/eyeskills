/*
    This code belongs to the "EyeSkills Framework". It is designed to assist
    in the development of software for the human/animal visual system.

    Copyright(C) 2018 Dr. Thomas Benjamin Senior, Michael Zöller.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

using UnityEngine;
using PowerUI;

namespace EyeSkills
{
    public class UsernameUI : MonoBehaviour
    {
        public string[] assetOptions;
        public string[] lockExperienceOptions;

        void Start()
        {
            UI.document.getElementById("practitioner").setAttribute("value", PlayerPrefs.GetString("EyeSkills.Practitioner"));
            UI.document.getElementById("name").setAttribute("value", PlayerPrefs.GetString("EyeSkills.Name"));
            UI.document.getElementById("remoteServerUrl").setAttribute("value", PlayerPrefs.GetString("EyeSkills.remoteServerUrl", "ws://remote.eyeskills.org:34567/"));
            if (PlayerPrefs.HasKey("EyeSkills.practitionerMode")){
                UI.document.getElementById("practitionerMode").setAttribute("value", PlayerPrefs.GetString("EyeSkills.practitionerMode"));
                UI.document.getElementById("practitionerMode").setAttribute("checked", PlayerPrefs.GetString("EyeSkills.practitionerMode"));
            } 
            string s = "";
            if (assetOptions != null)
                foreach (string name in assetOptions) {
                    var selected = PlayerPrefs.GetString("EyeSkills.assetStyle", "default") == name ? " selected" : "";
                    s += "<option value=\"" + name + "\"" + selected + ">" + name + "</option>";
                }
            UI.Variables["assetstyle"] = s;

            s = "<option value=\"-\">-</option>";
            if (lockExperienceOptions != null)
                foreach (string name in lockExperienceOptions) {
                    var selected = PlayerPrefs.GetString("EyeSkills.lockExperience") == name ? " selected" : "";
                    s += "<option value=\"" + name + "\"" + selected + ">" + name + "</option>";
                }
            UI.Variables["experienceoptions"] = s;
        }
    }
}
