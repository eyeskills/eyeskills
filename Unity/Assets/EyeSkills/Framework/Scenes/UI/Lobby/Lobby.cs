/*
    This code belongs to the "EyeSkills Framework". It is designed to assist
    in the development of software for the human/animal visual system.

    Copyright(C) 2018 Dr. Thomas Benjamin Senior, Michael Zöller.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections;
using UnityEngine;
using UnityEngine.XR;
using UnityEngine.SceneManagement;

namespace EyeSkills
{
    public class Lobby : MonoBehaviour
    {
        public float counter = 10f;

        private void Start()
        {
            Debug.Log("Lobby Start");
            //Screen.orientation = ScreenOrientation.LandscapeLeft;
            StartCoroutine(_go());
        }

        private IEnumerator _go()
        {
            Debug.Log("Starting Lobby Enumerator - next scene to load " +
                      ESSceneManager.instance.sceneToLoad.ToString());
            NetworkManager.instance.RegisterScene("Lobby", "");
            NetworkManager.instance.RegisterButton("start", "Start",
                "This button only has effect once the device is in landscape orientation.");


            var landscapeElement = PowerUI.UI.document.getElementById("landscape");
            var portraitElement = PowerUI.UI.document.getElementById("portrait");
            ScreenOrientation so = ScreenOrientation.Landscape;

            while (true)
            {
                if (so != Screen.orientation)
                {
                    Debug.Log("Lobby displaying screen");
                    bool landscape = (Screen.orientation == ScreenOrientation.LandscapeLeft ||
                                      Screen.orientation == ScreenOrientation.LandscapeRight);
                    landscapeElement.style.display = landscape ? "block" : "none";
                    portraitElement.style.display = landscape ? "none" : "block";
                    NetworkManager.instance.RegisterScene("Lobby",
                        "Device is in " + Screen.orientation.ToString() + " orientation.");
                    so = Screen.orientation;
                }

                if (Screen.orientation == ScreenOrientation.Landscape ||
                    Screen.orientation == ScreenOrientation.LandscapeLeft ||
                    Screen.orientation == ScreenOrientation.LandscapeRight)
                    break;

                yield return 0;
            }

            // hide UI
            GetComponent<PowerUI.Manager>().enabled = false;

            // turn on VR
            XRSettings.LoadDeviceByName("cardboard");
            yield return 0;
            XRSettings.enabled = true;
            yield return 0;

            Debug.Log("Lobby loading scene " + ESSceneManager.instance.sceneToLoad.ToString());

            SceneManager.LoadScene(ESSceneManager.instance.sceneToLoad);
        }
    }
}