/*
    This code belongs to the "EyeSkills Framework". It is designed to assist
    in the development of software for the human/animal visual system.

    Copyright(C) 2018 Dr. Thomas Benjamin Senior, Michael Zöller.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
using UnityEngine;

namespace EyeSkills.ImageEffects
{
    [ExecuteInEditMode]
    public class Brightness : UnityStandardAssets.ImageEffects.ImageEffectBase
    {
        [Range(0f, 1.0f)] public float brightness = 1f;
        private float a = 1f / 100f;

        // Modifies the rate of darkening
        public float rangeSensitivity = 1.2f;

        // Called by camera to apply image effect
        void OnRenderImage(RenderTexture source, RenderTexture destination)
        {
            //Brightness of 0 is full brightness. 1 should be dark.
            float adaptedBrightness = Mathf.Pow(a, brightness*rangeSensitivity); // Make the drop off towards black far slower.
            material.SetFloat("_Brightness", adaptedBrightness);
            Graphics.Blit(source, destination, material);
        }
    }
}