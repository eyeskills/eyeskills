/*
    This code belongs to the "EyeSkills Framework". It is designed to assist
    in the development of software for the human/animal visual system. 

    Copyright(C) 2018 Dr. Thomas Benjamin Senior, Michael Zöller.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

using UnityEngine;
using UnityEditor;
using System.IO;
using System.Linq;

namespace EyeSkills.Editor
{
    public class BuildLogic : MonoBehaviour
    {
        [UnityEditor.MenuItem("EyeSkills/Android Build")]
        static void androidBuild()
        {
            var target = BuildTarget.Android;
            var options = BuildOptions.None;

            string[] scenes = EditorBuildSettings.scenes.Where(s => s.enabled).Select(s => s.path).ToArray();

            var outputFile = Application.dataPath + "/../Build/EyeSkills.apk";
            if (File.Exists(outputFile))
                File.Delete(outputFile);

            Debug.Log(string.Format("APK path: \"{0}\"", outputFile));
            for (int i = 0; i < scenes.Length; ++i)
                Debug.Log(string.Format("Scene[{0}]: \"{1}\"", i, scenes[i]));

            Debug.Log("Starting Android build...");
            BuildPipeline.BuildPlayer(scenes, outputFile, target, options);
        }
    }
}