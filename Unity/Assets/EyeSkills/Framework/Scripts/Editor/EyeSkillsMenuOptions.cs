/*
    This code belongs to the "EyeSkills Framework". It is designed to assist
    in the development of software for the human/animal visual system. 

    Copyright(C) 2018 Dr. Thomas Benjamin Senior, Michael Zöller.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

 using UnityEngine;
using UnityEditor;

namespace EyeSkills.Editor
{
    public class EyeSkillsMenuOptions
    {
        [MenuItem("EyeSkills/(Re-)Create all speech files")]
        private static void CreateSpeechFiles()
        {
            CreateAudioFiles createAudioFiles = new CreateAudioFiles();
            createAudioFiles.ConvertTextFilesToSpeech("Assets/EyeSkills/Framework/Resources/Audio", false);
        }

        [MenuItem("EyeSkills/Create new speech files")]
        private static void CreateNewSpeechFiles()
        {
            CreateAudioFiles createAudioFiles = new CreateAudioFiles();
            createAudioFiles.ConvertTextFilesToSpeech("Assets/EyeSkills/Framework/Resources/Audio", true);
        }
    }
}