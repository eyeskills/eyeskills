/*
    This code belongs to the "EyeSkills Framework". It is designed to assist
    in the development of software for the human/animal visual system. 

    Copyright(C) 2018 Dr. Thomas Benjamin Senior, Michael Zöller.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using UnityEditor;
using System.IO;
using System.Diagnostics;

namespace EyeSkills.Editor
{
    public class CreateAudioFiles
    {
        /// <summary>
        /// Converts text files in a directory to speech file in the format m4a using OSX's "say" command.
        /// </summary>
        /// <param name="dirPath">Dir path.</param>
        public void ConvertTextFilesToSpeech(string dirPath, bool onlyTextFilesMissingAudio)
        {
            DirectoryInfo dirInfo = new DirectoryInfo(dirPath);

            FileInfo[] fileInf = dirInfo.GetFiles("*.txt");

            foreach (FileInfo fileInfo in fileInf)
            {
                //UnityEngine.Debug.Log("File found " + fileInfo.Name);
                string inputFilePath = fileInfo.FullName;
                string key = Path.GetFileNameWithoutExtension(inputFilePath);
                string outputDirPath = Path.GetDirectoryName(inputFilePath);
                string outputFilePath = outputDirPath + "/" + key + ".aiff";

                if ((onlyTextFilesMissingAudio && !System.IO.File.Exists(outputFilePath)) || !onlyTextFilesMissingAudio)
                {
                    UnityEngine.Debug.Log("Creating audio file for " + fileInfo.Name);
                    StreamReader reader = new StreamReader(inputFilePath);
                    string audioText = reader.ReadToEnd();
                    reader.Close();

                    SpeakToFile3("Daniel", audioText, outputFilePath, outputDirPath);
                    //coroutine = SpeakToFile3("Daniel", audioText, outputFilePath, outputDirPath);
                    //StartCoroutine(coroutine);    
                }
                else
                {
                    //UnityEngine.Debug.Log("Ignoring as audio file already exists");
                }
            }
        }

        /// <summary>
        /// Speaks to file3.  Third attempt to get this osx specific call to "say" working.
        /// </summary>
        /// <returns>The to file3.</returns>
        /// <param name="voice">Voice.</param>
        /// <param name="inputText">Input text.</param>
        /// <param name="outputFile">Output file.</param>
        /// <param name="outputDirPath">Output dir path.</param>
        public void SpeakToFile3(string voice, string inputText, string outputFile, string outputDirPath)
        {
            string arguments = "-v " + voice + " '" + inputText + "' -o " + outputFile;

            ProcessStartInfo processInfo = new ProcessStartInfo("/usr/bin/say", arguments);
            processInfo.UseShellExecute = false;

            var process = Process.Start(processInfo);
            process.WaitForExit();
            process.Close();
        }
    }
}