/*
    This code belongs to the "EyeSkills Framework". It is designed to assist
    in the development of software for the human/animal visual system. 

    Copyright(C) 2018 Dr. Thomas Benjamin Senior, Michael Zöller.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EyeSkills;

namespace EyeSkills
{
    public interface IImageIntensityRatioController
    {
        float getDirection();
    }

    public class EyeSkillsVRHeadsetInput
    {
        private GameObject trackedCamera;
        private EyeSkillsInput esInput;
        private Quaternion initialRotation;

        private Quaternion currentRotation;

        //Make these variables public and putting them at the class level for simpler inspection
        private Quaternion relativeRotation;
        private float normalisedDirection;
        private Vector3 euler;

        /// <summary>
        /// The direction of gaze, right is +ve, left if -ve.  This is normalised between 0-1 mapped from the 180deg right/left.
        /// </summary>
        private float direction;

        /// <summary>
        /// We record the intial direction of gaze, and report anything to the 180deg "right" of this as a direction,
        /// normalised to 1, and vice versa for left;
        /// </summary>
        public EyeSkillsVRHeadsetInput(GameObject trackedCamera)
        {
            this.trackedCamera = trackedCamera;
            initialRotation = trackedCamera.transform.localRotation;
        }

        private float calculateDirectionFromRotation(Quaternion initialRotation, Quaternion currentRotation)
        {
            relativeRotation = Quaternion.Inverse(initialRotation) * currentRotation;

            euler = relativeRotation.eulerAngles;

            if (euler.x > 180)
            {
                euler.x = euler.x - 360; //we want to map continuous 0 -> 360 euler angles to 0->1 going right and 0->-1 going left.
            }

            float restrictMovementToDegrees = 45;
            normalisedDirection = (euler.x / restrictMovementToDegrees); //Everything above 1 or beneath -1 can be clamped by the consumer.

            // But now we only want to move the head a third of that distance to get maximum travel (not 180deg) so...
            //normalisedDirection = normalisedDirection * 6; // It will clamp out above that.

            return normalisedDirection;
        }

        /// <summary>
        /// Gets the direction. The direction of gaze, right is +ve, left if -ve.  This is normalised between 0-1 mapped from the 180deg right/left.
        /// </summary>
        /// <returns>The direction.</returns>
        public float getDirection()
        {
            currentRotation = trackedCamera.transform.localRotation;
            float normalisedDirection = calculateDirectionFromRotation(initialRotation, currentRotation);
            return normalisedDirection;
        }
    }
}