/*
    This code belongs to the "EyeSkills Framework". It is designed to assist
    in the development of software for the human/animal visual system. 

    Copyright(C) 2018 Dr. Thomas Benjamin Senior.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EyeSkills;

namespace EyeSkills
{

    /// <summary>
    /// Watching for hold long (within a threshold) the headset has remained still (used for selection)
    /// </summary>
    public class EyeSkillsVRHeadsetSelectByStillness : MonoBehaviour
    {
        /// <summary>
        /// The camera reflecting the position of the head.
        /// </summary>
        public GameObject trackedCamera;

        /// <summary>
        /// The degree to which the largest sequential reading within a time window may drift apart before we consider the movement to no longer be still.
        /// </summary>
        public float sequentialSensitivity = 2f;

        /// <summary>
        /// The degree to which the average movement over the measured time window may drift (e.g. the head could move very slowly - within the sequentialSensitivity threshold - quite far away
        /// </summary>
        public float averageSensitivity = 3f;

        /// <summary>
        /// The sampling frequency for reading the headset position in seconds.
        /// </summary>
        public float samplingFrequency = 0.2f;

        /// <summary>
        /// The maximum size of our monitoring queue. The length of time being monitored is maxQueueSize * samplingFrequency.
        /// </summary>
        private int maxQueueSize = 10;

        /// <summary>
        /// The last difference in angle between two readings of direction of gaze.
        /// </summary>
        private Queue<float> lastDifference = new Queue<float>();

        /// <summary>
        /// The initial gaze direction.
        /// </summary>
        private Quaternion lastGazeDirection;

        /// <summary>
        /// The still time.
        /// </summary>
        private float stillTime = 0f;

        private IEnumerator coroutine;

        private float firstTime;

        private bool saidSelecting = false;

        /// <summary>
        /// The initial activation delay in seconds. The time before the selector becomes active.
        /// </summary>
        public float initialActivationDelayInSeconds = 3f;

        /// <summary>
        /// The delay after cancellation.  Gives the user some time to make another deliberate choice to stay still.
        /// </summary>
        public float delayAfterCancellation = 2f;

        private void GetGazeAngle(){
            lastGazeDirection = trackedCamera.transform.localRotation;
        }

        public void Start()
        {
            stillTime = 0f;
            coroutine = TrackStillness();
            StartCoroutine(coroutine);
        }

        /// <summary>
        /// Are the thresholds for stillness exceeded. We check both the indidual and collective thresholds.
        /// </summary>
        /// <returns><c>true</c>, if thresholds exceeded was ared, <c>false</c> otherwise.</returns>
        /// <param name="q">Q.</param>
        private bool AreThresholdsExceeded(Queue<float> q){
            float drift = 0;
            foreach (float i in q){
                if (i > sequentialSensitivity) {
                    //Debug.Log("sequentialSensitivity threshold exceeded " + i);
                    return true;
                }
                drift += i;
            }
            if (drift > averageSensitivity) {
                Debug.Log("averageSensitivity threshold exceeded " + drift);
                return true;
            }
            //Debug.Log("Thresholds not exceeded ");
            return false;
        }

        IEnumerator TrackStillness(){

            //Give the user some time to orient themselves
            yield return new WaitForSeconds(initialActivationDelayInSeconds);

            //Initialise
            GetGazeAngle();
            firstTime = Time.time;

            //Debug.Log("Starting TrackStillness");
            while (true){
            
                //Calculate the difference in readings.
                float lastGazeDifference = Quaternion.Angle(lastGazeDirection, trackedCamera.transform.localRotation);
                lastDifference.Enqueue(lastGazeDifference);
                //Debug.Log("Last Gaze difference was "+ lastGazeDifference);

                //We might also want to detect direction changes here. Add the direction to a similar queue.
                //If we count more than X direction changes across the period, we have a head shake.


                if (lastDifference.Count>maxQueueSize){
                    //Debug.Log("Shrinking Queue");
                    lastDifference.Dequeue(); 
                }

                //Calculate if we have exceeded any of our movement thresholds
                if (AreThresholdsExceeded(lastDifference)){
                    //Reset our stillTime and empty our queue
                    //Debug.Log("Resetting sillTime ");

                    if (stillTime > delayAfterCancellation){
                        AudioManager.instance.Say("cancellingSelection");
                    }

                    stillTime = 0;
                    firstTime = Time.time;
                    saidSelecting = false;
                    lastDifference.Clear();

                } else {

                    if (stillTime.Equals(delayAfterCancellation) && (saidSelecting==false))
                    {
                        AudioManager.instance.Say("selecting");
                        saidSelecting = true;
                    }

                    //Extend our stillTime
                    stillTime = (Time.time - firstTime);
                    //Debug.Log("Extending sillTime to "+stillTime);

                }

                GetGazeAngle();
                yield return new WaitForSeconds(samplingFrequency);
            }
        }

     
        /// <summary>
        /// Returns the amount of ms within which the headset has been still. The consumer can decide what to do with that information.
        /// </summary>
        /// <returns>The time still.</returns>
        public float getTimeStill()
        {
            if (stillTime<=delayAfterCancellation){
                return 0;
            } else {
                return stillTime - delayAfterCancellation;
            }

        }
    }
}