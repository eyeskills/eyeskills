﻿/*
    This code belongs to the "EyeSkills Framework". It is designed to assist
    in the development of software for the human/animal visual system. 

    Copyright(C) 2018 Dr. Thomas Benjamin Senior.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Selection indicator scaler. Given a game object to represent an indicator, we are told what percentage we ought to display.
/// </summary>
public class SelectionIndicatorScaler : MonoBehaviour {

    public GameObject indicatorLeft,indicatorRight;
    public float maxScale = 20f; 
    private float initialXPositionLeft = 0f, initialXPositionRight = 0f;
	// Use this for initialization
	void Start () {
        initialXPositionLeft = indicatorLeft.transform.localPosition.x;
        initialXPositionRight = indicatorRight.transform.localPosition.x;
        indicatorLeft.SetActive(true);
        indicatorRight.SetActive(true);
    }
	
    /// <summary>
    /// Sets the percentage displayed by the indicator. From 0 to 1.
    /// </summary>
    /// <param name="percentage">Percentage.</param>
    public void SetIndicatorPercentage(float percentage){
        //Debug.Log("Setting maxScale " + maxScale + " multiplied by "+ percentage);
        //indicator.transform.localScale = new Vector3(maxScale * percentage,1,1);
        indicatorLeft.transform.localPosition = new Vector3( (initialXPositionLeft + (maxScale * percentage)), indicatorLeft.transform.localPosition.y,indicatorLeft.transform.localPosition.z);
        indicatorRight.transform.localPosition = new Vector3((initialXPositionRight + (maxScale * percentage)), indicatorRight.transform.localPosition.y, indicatorRight.transform.localPosition.z);
    }

    public void Reset()
    {
        indicatorLeft.transform.localPosition = new Vector3(initialXPositionLeft, indicatorLeft.transform.localPosition.y, indicatorLeft.transform.localPosition.z);
        indicatorRight.transform.localPosition = new Vector3(initialXPositionRight, indicatorRight.transform.localPosition.y, indicatorRight.transform.localPosition.z);
    }

    /// <summary>
    /// Shows the selected state
    /// </summary>
    public void ShowSelected(){

    }
}
