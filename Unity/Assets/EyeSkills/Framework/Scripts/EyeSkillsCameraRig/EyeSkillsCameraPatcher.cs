/*
    This code belongs to the "EyeSkills Framework". It is designed to assist
    in the development of software for the human/animal visual system. 

    Copyright(C) 2018 Dr. Thomas Benjamin Senior, Michael Zöller.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SpatialTracking;

namespace EyeSkills
{
    public class EyeSkillsCameraPatcher : MonoBehaviour
    {
        [Tooltip("Reference to the disabled GvrInstantPreviewMain scene object")]
        public GameObject instantPreview;

        public bool patchInstantPreview;

        void Start()
        {
            StartCoroutine(_Start());
        }

        IEnumerator _Start()
        {
            //_patchCamera (StereoTargetEyeMask.Left);
            //_patchCamera (StereoTargetEyeMask.Right);

            //Camera.main.gameObject.GetComponent<Camera> ().enabled = false;

            //if (!patchInstantPreview)
            //			yield break;

            yield return new WaitForSeconds(1);

            // only activate google instant preview after camera setup is finished
            instantPreview.SetActive(true);

            // yield one frame to make sure googles instant preview sets up all game objects
            yield return 0;

            // disable instant preview cameras for editor main camera (mono)
            GameObject.Find("Main Camera:Instant Preview Left").SetActive(false);
            GameObject.Find("Main Camera:Instant Preview Right").SetActive(false);

            // disable instant preview cameras for left respkt right cameras
            GameObject.Find("EyeSkills Camera Left:Instant Preview Right").SetActive(false);
            GameObject.Find("EyeSkills Camera Right:Instant Preview Left").SetActive(false);
        }

        void _patchCamera(StereoTargetEyeMask eye)
        {
            // create empty game object we use to rotate eye
            GameObject offset = new GameObject("Eyeskills Offset " + eye);
            // use SetParent to make transform keep its local orientation
            offset.transform.SetParent(Camera.main.transform, false);

            // create single eye camera based on scenes main camera
            GameObject camera = new GameObject("EyeSkills Camera " + eye);
            camera.transform.SetParent(offset.transform, false);

            camera.AddComponent<Camera>();
            camera.GetComponent<Camera>().CopyFrom(Camera.main);
            camera.GetComponent<Camera>().stereoTargetEye = eye;

            camera.AddComponent<TrackedPoseDriver>();
        }
    }
}