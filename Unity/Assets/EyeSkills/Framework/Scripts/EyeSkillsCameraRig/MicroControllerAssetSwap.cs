/*
    This code belongs to the "EyeSkills Framework". It is designed to assist
    in the development of software for the human/animal visual system. 

    Copyright(C) 2018 Dr. Thomas Benjamin Senior, Michael Zöller.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EyeSkills;

namespace EyeSkills
{
    /// <summary>
    /// Micro controller XCues.  Introduce some visual cues (at different locations and preferably with different colours) to help the participant report whether or not they are seeing with both eys or only one.
    /// Add this as a child script of the EyeSkills Camera Rig in the Unity Editor.
    /// </summary>
    public class MicroControllerAssetSwap : MicroController
    {
        protected AudioManager audioManager;
        private EyeSkillsCameraRigEditMode parentManager;
        private EyeSkillsInput esInput;
        private bool active = false;
        public AssetSwitcher assetSwitcher;

        public void Start()
        {
            audioManager = AudioManager.instance;
            esInput = EyeSkillsInput.instance;
        }

        public override void Shutdown()
        {
            active = false;
            parentManager.MicroControllerShutdown();
        }

        public override void Startup(EyeSkillsCameraRigEditMode _parent)
        {
            active = true;
            audioManager.Say("AssetSwapAvailable");
            parentManager = _parent;
        }


        public void Update()
        {
            /*
             * At the very least, this needs a list of possible states we can enter into in this scene.
             * It should hunt through the camera object to build a list of them.
             * Moving the cursor up/down moves through that list... we just say "next/last asset".
             */

            if (active)
            {
                if (esInput.GetShortButtonPress("EyeSkills Up"))
                {
                    Debug.Log("Next asset Switch");
                    audioManager.Say("SwappingAsset");
                    assetSwitcher.SwitchAssetsToNext();
                    //Shutdown();
                }
                else if (esInput.GetShortButtonPress("EyeSkills Confirm") || (esInput.GetShortButtonPress("EyeSkills Cancel")))
                {
                    Shutdown();
                }               
            }
        }
    }
}