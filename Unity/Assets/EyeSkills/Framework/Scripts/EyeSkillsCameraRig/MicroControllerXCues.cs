/*
    This code belongs to the "EyeSkills Framework". It is designed to assist
    in the development of software for the human/animal visual system. 

    Copyright(C) 2018 Dr. Thomas Benjamin Senior, Michael Zöller.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EyeSkills;

namespace EyeSkills
{
    /// <summary>
    /// Micro controller XCues.  Introduce some visual cues (at different locations and preferably with different colours) to help the participant report whether or not they are seeing with both eys or only one.
    /// Add this as a child script of the EyeSkills Camera Rig in the Unity Editor.
    /// </summary>
    public class MicroControllerXCues : MicroController
    {
        protected AudioManager audioManager;
        private EyeSkillsCameraRigEditMode parentManager;
        public GameObject leftCue, rightCue;
        private bool active = false;

        public void Start()
        {
            deactivateCues();
            audioManager = AudioManager.instance;
        }

        public override void Shutdown()
        {
            active = false;
            //deactivateCues();
            parentManager.MicroControllerShutdown();
        }

        public override void Startup(EyeSkillsCameraRigEditMode _parent)
        {
            active = true;
            audioManager.Say("XCuesAvailable");
            parentManager = _parent;
        }

        private void activateCues()
        {
            leftCue.SetActive(true);
            rightCue.SetActive(true);
        }

        private void deactivateCues()
        {
            leftCue.SetActive(false);
            rightCue.SetActive(false);
        }

        public void Update()
        {
            // Here we have our actual control interface - to add and remove our cues.
            // This is really annoying having this extra Update logic here, but enabling/disabling the script was making
            // life really complex- it doesn't happen instantaneously, but requires waiting for a full iteration AFAIK
            if (active)
            {
                if (Input.GetButton("EyeSkills Up"))
                {
                    Debug.Log("Activating Cues");
                    activateCues();
                }
                else if (Input.GetButton("EyeSkills Down"))
                {
                    Debug.Log("Deactivating Cues");
                    deactivateCues();
                }
                else if (Input.GetButton("EyeSkills Confirm") || (Input.GetButton("EyeSkills Cancel")))
                {
                    Shutdown();
                }
            }
        }
    }
}