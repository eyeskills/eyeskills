/*
    This code belongs to the "EyeSkills Framework". It is designed to assist
    in the development of software for the human/animal visual system.

    Copyright(C) 2018 Dr. Thomas Benjamin Senior, Michael Zöller.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace EyeSkills
{
    /// <summary>
    /// Conflict zone model. Add or remove a source of retinal conflict.
    /// </summary>
    public class ConflictZoneModel : MonoBehaviour
    {
        private float brightnessRatio;

        /// <summary>
        /// A container for something you wish to display and alter the luminance of.
        /// </summary>
        public GameObject leftConflictItem, rightConflictItem;

        public GameObject blinkerLeft, blinkerRight;

        /// <summary>
        /// Show or hide the conflict background.
        /// </summary>
        /// <returns>The show.</returns>
        /// <param name="showing">If set to <c>true</c> showing.</param>
        public void Show(bool showing)
        {
            GameObject[] conflictObjects = GameObject.FindGameObjectsWithTag("Conflict");
            if( conflictObjects.Length > 0 ) {
              foreach(GameObject go in conflictObjects)
                go.SetActive(showing);
            } else {
              leftConflictItem.SetActive(showing);
              rightConflictItem.SetActive(showing);
            }
        }

        public void SetLuminanceRatio(float brightnessRatio){
            FindObjectOfType<EyeSkillsCameraRig>().SetBinocularSuppressionRatio(brightnessRatio);
        }

        /// <summary>
        /// Moves the left and right (eye) items into conflict
        /// </summary>
        public void IntoConflict()
        {
            blinkerLeft.SetActive(false);
            blinkerRight.SetActive(false);
        }

        /// <summary>
        /// Moves the left and right (eye) items out of conflict
        /// </summary>
        public void OutOfConflict()
        {
          blinkerLeft.SetActive(true);
          blinkerRight.SetActive(true);
        }

    }
}
