/*
    This code belongs to the "EyeSkills Framework". It is designed to assist
    in the development of software for the human/animal visual system.

    Copyright(C) 2018 Dr. Thomas Benjamin Senior, Michael Zöller.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EyeSkills;

namespace EyeSkills
{
    /// <summary>
    /// Micro controller conflict. This controller actually performs two functions - enabling the activation of conflict, but also control over the binocular suppression ratio.
    /// Add this as a child script of the EyeSkills Camera Rig in the Unity Editor.
    /// </summary>
    public class MicroControllerConflict : MicroController
    {
        protected AudioManager audioManager;
        private EyeSkillsCameraRigEditMode parentManager;
        private bool active = false;
        private float brightnessRatio = 0f;
        public float brightnessRatioInc = 0.01f;

        private EyeSkillsCameraRig cameraRig;
        private ConflictZoneModel conflictZoneModel;

        public void Start()
        {
            if( conflictZoneModel == null)
              conflictZoneModel = GetComponent<ConflictZoneModel>();
                            audioManager = AudioManager.instance;
              cameraRig = GetComponent<EyeSkillsCameraRig>();

            deactivateConflict();
        }

        public override void Shutdown()
        {
            active = false;
            //deactivateConflict();
            parentManager.MicroControllerShutdown();
        }

        public override void Startup(EyeSkillsCameraRigEditMode _parent)
        {
            active = true;
            audioManager.Say("ConflictAvailable");
            parentManager = _parent;
        }

        private void activateConflict()
        {
            conflictZoneModel.Show(true);
            conflictZoneModel.IntoConflict();
        }

        private void deactivateConflict()
        {
            conflictZoneModel.Show(false);
        }

        /// <summary>
        /// Alters the luminance ratio. From -1 to 1
        /// </summary>
        /// <param name="_brightnessRatio">Brightness ratio.</param>
        private void alterLuminanceRatio(float _brightnessRatio)
        {
            Debug.Log("MicroControllerConflict setting brightnessRatio to " + _brightnessRatio);

            // float d = 0.5f * _brightnessRatio;
            // float d1 = 0.5f + d;
            // float d2 = 0.5f - d;
            //
            // conflictZoneModel.SetLeftEyeActiveColor(new Color(1f, 1f, 1f, d1));
            // conflictZoneModel.SetRightEyeActiveColor(new Color(1f, 1f, 1f, d2));
            conflictZoneModel.SetLuminanceRatio(_brightnessRatio);
        }

        public void Update()
        {
            // Here we have our actual control interface - to add and remove our cues.
            // This is really annoying having this extra Update logic here, but enabling/disabling the script was making
            // life really complex- it doesn't happen instantaneously, but requires waiting for a full iteration AFAIK
            if (active)
            {
                if (Input.GetButton("EyeSkills Up"))
                {
                    Debug.Log("Activating Conflict");
                    activateConflict();
                }
                else if (Input.GetButton("EyeSkills Down"))
                {
                    Debug.Log("Deactivating Conflict");
                    deactivateConflict();
                }
                else if (Input.GetButton("EyeSkills Right"))
                {
                    brightnessRatio = Mathf.Clamp(brightnessRatio + brightnessRatioInc, -1f, 1f);
                    alterLuminanceRatio(brightnessRatio);
                }
                else if (Input.GetButton("EyeSkills Left"))
                {
                    brightnessRatio = Mathf.Clamp(brightnessRatio - brightnessRatioInc, -1f, 1f);
                    alterLuminanceRatio(brightnessRatio);
                }
                else if (Input.GetButton("EyeSkills Confirm") || (Input.GetButton("EyeSkills Cancel")))
                {
                    Shutdown();
                }
            }
        }
    }
}
