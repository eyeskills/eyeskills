/*
    This code belongs to the "EyeSkills Framework". It is designed to assist
    in the development of software for the human/animal visual system. 

    Copyright(C) 2018 Dr. Thomas Benjamin Senior, Michael Zöller.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EyeSkills;

namespace EyeSkills
{
    /// <summary>
    /// Micro controller brightness.  This manually alters the binocular suppression ratio in the EyeSkills CameraRig.
    /// Add this as a child script of the EyeSkills Camera Rig in the Unity Editor.
    /// </summary>
    public class MicroControllerBrightness : MicroController
    {
        protected AudioManager audioManager;
        private EyeSkillsCameraRigEditMode parentManager;
        public GameObject leftEye, rightEye;
        private bool active = false;
        private EyeSkillsCameraRig cameraRig;
        private float ratio = 0;
        public float ratioIncrement = 0.01f;

        public void Start()
        {
            audioManager = AudioManager.instance;
            cameraRig = gameObject.GetComponent<EyeSkillsCameraRig>();
        }

        public override void Shutdown()
        {
            active = false;
            //leftEye.transform.Rotate(Vector3.forward - leftEye.transform.eulerAngles);
            //rightEye.transform.Rotate(Vector3.forward - rightEye.transform.eulerAngles);
            parentManager.MicroControllerShutdown();
        }

        public override void Startup(EyeSkillsCameraRigEditMode _parent)
        {
            active = true;
            audioManager.Say("BinocularSuppressionRatioAvailable");
            parentManager = _parent;
        }

        /// <summary>
        /// Alters the camera ratio. 1 is full right zero left, -1 is full left and zero right.
        /// 0 is 0.5 for each eye.  What if we want both cameras to behave normally? 
        /// Should we actually use the boost function to 2 starting from brightness 1 in each eye? 
        /// That would imply that, in our binocular suppression scene, we need to set the brightess/color of the material to 0.5 to start with.
        /// If we use "boost" we may also oversaturate elements in the scene. 
        /// How do we apply it now?!?
        /// Ah, it actually applies it from 0 - 2 using that brightness script inside the CameraRig. That also needs to use the new internal function
        /// ================
        /// SO : We make everything consistent and use the brightness script from 0 - 1 which means adding an internal method to the camera, modifying the camera init, 
        /// changing the binocular suppression scene, adding a micro controller, and testing.
        /// ================
        /// </summary>
        /// <param name="ratio">Ratio.</param>
        private void alterSuppressionRatio(float ratio)
        {
            cameraRig.SetBinocularSuppressionRatio(ratio);
        }

        public void Update()
        {
            if (active)
            {
                if (Input.GetButton("EyeSkills Confirm") || (Input.GetButton("EyeSkills Cancel")))
                {
                    //Save the new ratio (?!?) -> Probably in a scene specific way - need to implement
                    Shutdown();
                }


                if (Input.GetButton("EyeSkills Right"))
                {
                    Debug.Log("Increasing ratio...");
                    ratio = Mathf.Clamp(ratio + ratioIncrement, -1f, 1f);
                    alterSuppressionRatio(ratio);
                }
                else if (Input.GetButton("EyeSkills Left"))
                {
                    Debug.Log("Decreasing ratio...");
                    ratio = Mathf.Clamp(ratio - ratioIncrement, -1f, 1f);
                    alterSuppressionRatio(ratio);
                }
            }
        }
    }
}