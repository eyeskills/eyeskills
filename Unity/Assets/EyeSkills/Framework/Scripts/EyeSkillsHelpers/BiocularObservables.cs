/*
    This code belongs to the "EyeSkills Framework". It is designed to assist
    in the development of software for the human/animal visual system. 

    Copyright(C) 2018 Dr. Thomas Benjamin Senior, Michael Zöller.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EyeSkills
{
    public class BiocularObservables : MonoBehaviour
    {
        //Taken from Monocular Suppression Controller - I wish we had mixins.
        public MultiFixationSprite rightOccluder, leftOccluder;

        protected MultiFixationSprite currentOcculder;
        protected int fixIndex = 1;

        /// <summary>
        /// Which side to apply commands to. -1 is left, 0 is both, 1 is right.
        /// </summary>
        protected int applyCommandsToSide = 0;

        /// <summary>
        /// Hardcoded for two images. Just extend this to wrap around in sequence
        /// </summary>
        public void PickAlternateImage()
        {
            if (fixIndex == 1)
            {
                fixIndex = 0;
            }
            else
            {
                fixIndex = 1;
            }

            leftOccluder.PickByIndex(fixIndex);
            rightOccluder.PickByIndex(fixIndex);
        }

        public float AlterDistanceLeft(float startingDistance, bool v)
        {
            return leftOccluder.AlterDistance(startingDistance, v);
        }

        public float AlterDistanceRight(float startingDistance, bool v)
        {
            return rightOccluder.AlterDistance(startingDistance, v);
        }

        /// <summary>
        /// Alters the distance. Return value is potentially ambiguous.
        /// </summary>
        /// <returns>The distance.</returns>
        /// <param name="startingDistance">Starting distance.</param>
        /// <param name="v">If set to <c>true</c> v.</param>
        public float AlterDistance(float startingDistance, bool v)
        {
            float distance = 0;
            if (applyCommandsToSide == -1 || applyCommandsToSide == 0)
            {
                distance = AlterDistanceLeft(startingDistance, v);
            }

            if (applyCommandsToSide == 1 || applyCommandsToSide == 0)
            {
                distance = AlterDistanceRight(startingDistance, v);
            }

            return distance;
        }

        public void ApplyCommandsToLeftSide()
        {
            applyCommandsToSide = -1;
        }

        public void ApplyCommandsToRightSide()
        {
            applyCommandsToSide = 1;
        }

        public void ApplyCommandsToBothSides()
        {
            applyCommandsToSide = 0;
        }
    }
}