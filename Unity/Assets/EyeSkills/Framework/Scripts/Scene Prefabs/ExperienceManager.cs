/*
    This code belongs to the "EyeSkills Framework". It is designed to assist
    in the development of software for the human/animal visual system. 

    Copyright (C) 2018  Dr. Thomas Benjamin Senior, Michael Andreas Zöller.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace EyeSkills.Experiences
{
    public class ExperienceManager : MonoBehaviour
    {
        public GameObject healthBar;
        public float health = 1f;

        public float speedIncreaseDelay;
        public float speedIncreasePerSecond;

        // singleton like access pattern
        public static ExperienceManager instance;

        void Awake()
        {
            if (instance == null)
                instance = this;
            else if (instance != this)
                Destroy(gameObject);

            ESSceneManager.instance.VRSwitch(true);
            if (!GameObject.Find("SceneManager"))
            {
                Debug.Log("Bootstrap not loaded. Loading...");
                SceneManager.LoadScene("Bootstrap", LoadSceneMode.Additive);
            }
        }

        private void Start()
        {
            if (speedIncreasePerSecond > 0)
                StartCoroutine(_speedIncrease());
        }

        IEnumerator _speedIncrease()
        {
            yield return new WaitForSeconds(speedIncreaseDelay);

            while (true)
            {
                Time.timeScale += speedIncreasePerSecond;
                yield return new WaitForSeconds(1f);
            }
        }

        public void changeHealth(float amount)
        {
            if (health + amount < 0)
            {
                Time.timeScale = 1f;
                Debug.Log("Player died. Scene time: " + Time.timeSinceLevelLoad);
                SceneManager.LoadScene("Menu");
            }

            health = Mathf.Clamp01(health + amount);

            Vector3 scale = healthBar.transform.localScale;
            scale.z = health * 2;
            healthBar.transform.localScale = scale;
        }
    }
}