/*
    This code belongs to the "EyeSkills Framework". It is designed to assist
    in the development of software for the human/animal visual system. 

    Copyright(C) 2018 Dr. Thomas Benjamin Senior, Michael Zöller.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

using UnityEngine;
using UnityEngine.SceneManagement;

namespace EyeSkills.Calibrations
{
    public class EyeSkillsCalibration : MonoBehaviour
    {
        private void Awake()
        {
            if (ESSceneManager.instance)
                ESSceneManager.instance.VRSwitch(true);

            if (GameObject.Find("SceneManager")) return;

            Debug.Log("Bootstrap not loaded. Loading...");
            SceneManager.LoadScene("Bootstrap", LoadSceneMode.Additive);
        }
    }
}