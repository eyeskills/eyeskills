/*
    This code belongs to the "EyeSkills Framework". It is designed to assist
    in the development of software for the human/animal visual system. 

    Copyright(C) 2018 Dr. Thomas Benjamin Senior, Michael Zöller.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EyeSkills
{
    /// <summary>
    /// EyeSkills Audio Manager.  Simplifies the loading and playing of pre-generated audio resources.
    /// </summary>
    public class AudioManager : MonoBehaviour
    {
        public string language = "en";
        public string ttsAudioDir = "Audio";

        AudioSource audioSource;
        AudioClip notFoundClip;

        void Start()
        {
            DontDestroyOnLoad(this.gameObject);
        }

        // singleton like access pattern
        public static AudioManager instance = null;

        void Awake()
        {
            audioSource = GetComponent<AudioSource>();
            notFoundClip = Resources.Load(ttsAudioDir + "/DoesNotExist") as AudioClip;

            if (instance == null)
                instance = this;
            else if (instance != this)
                Destroy(gameObject);
        }

        public void Say(string key)
        {
            string audioFile = ttsAudioDir + "/" + language + "-" + key;
            //string audioFile = ttsAudioDir + "/" + key;
            //Debug.Log("Looking for " + audioFile);

            AudioClip clip = null;
            try
            {
                clip = Resources.Load<AudioClip>(audioFile);
            }
            catch (UnityException e)
            {
                Debug.LogWarning(e);
            }

            if (clip == null)
                audioSource.clip = notFoundClip;
            else
                audioSource.clip = clip;

            audioSource.Play();
        }
    }
}