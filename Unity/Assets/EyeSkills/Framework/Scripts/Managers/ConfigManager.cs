/*
    This code belongs to the "EyeSkills Framework". It is designed to assist
    in the development of software for the human/animal visual system.

    Copyright(C) 2018 Dr. Thomas Benjamin Senior, Michael Zöller.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.SceneManagement;

using UnityEngine.XR;

namespace EyeSkills
{
    public class ConfigBase
    {
        public string id()
        {
            return GetType().FullName;
        }

        public string PlayerPrefsId()
        {
            string myId="EyeSkills.Config-" + NetworkManager.instance.GetUUID() + "-" + id();
            return myId;
        }

        public ConfigBase()
        {

            //Debug.Log("AHA " + id());
            // constructor is called during serialization! see LoadConfig
            if (ConfigManager.instance)
                ConfigManager.instance.LoadConfig(this);
        }

        public void Load()
        {
            // populate from PlayerPrefs
            var json = PlayerPrefs.GetString(PlayerPrefsId());
            if (json != "")
                JsonConvert.PopulateObject(json, this);

            // request network config
            NetworkManager.instance.Send("request_config", "{\"id\": \"" + id() + "\"}");
        }

        public void Save()
        {
            if (ConfigManager.instance)
                ConfigManager.instance.SaveConfig(this);
        }

        public void Sync()
        {
            if (ConfigManager.instance)
                ConfigManager.instance.SaveConfig(this, true);
        }
    }

    public class ConfigBasePerScene : ConfigBase
    {
        public new string id()
        {
            return GetType().FullName + SceneManager.GetActiveScene();
        }
    }

    public class ConfigManager : MonoBehaviour
    {
        // singleton like access pattern
        public static ConfigManager instance;

        private void Awake()
        {
            // singleton like access pattern
            if (instance == null)
                instance = this;
            else if (instance != this)
                Destroy(gameObject);

            DontDestroyOnLoad(this);
        }

        Dictionary<string, ConfigBase> ConfigBases = new Dictionary<string, ConfigBase>();

        void OnEnable()
        {
            SceneManager.sceneLoaded += OnSceneLoaded;
            SceneManager.sceneUnloaded += OnSceneUnloaded;
        }

        void OnDisable()
        {
            SceneManager.sceneLoaded -= OnSceneLoaded;
            SceneManager.sceneUnloaded -= OnSceneUnloaded;
        }

        void OnSceneUnloaded(Scene scene)
        {
            Debug.Log("OnSceneUnloaded: " + scene.name);

            if (scene.name == "Menu" || scene.name == "Bootstrap" || scene.name == "Lobby" )//|| !XRSettings.enabled)
                return;

            foreach (KeyValuePair<string, ConfigBase> entry in ConfigBases)
                SaveConfig(entry.Value);

            ConfigBases.Clear();
        }

        void OnSceneLoaded(Scene scene, LoadSceneMode mode)
        {
            Debug.Log("OnSceneLoaded: " + scene.name);

            if (scene.name == "Menu" || scene.name == "Bootstrap" || scene.name == "Lobby")
                return;

            foreach (KeyValuePair<string, ConfigBase> entry in ConfigBases)
                entry.Value.Load();
        }

        public void LoadConfig(ConfigBase config)
        {
            // PlayerPrefs needs to be used from main thread, so we postphone and resume in OnSceneLoaded
            ConfigBases[config.id()] = config;
        }

        public void SaveConfig(ConfigBase config, bool sync = false)
        {
            var json = JsonConvert.SerializeObject(config);
            Debug.Log("SAVE: " + json);

            if (!sync)
            {
                Debug.Log("Config: Saving config " + config.id());
                PlayerPrefs.SetString(config.PlayerPrefsId(), json);
            }
            else
                Debug.Log("Config: Syncing with server.");

            // send config to server
            NetworkManager.instance.Send("set_config", "{\"id\": \"" + config.id() + "\", \"config\": " + json + "}");
        }

        public void ConfigReply(string id, string config)
        {
            Debug.Log("Config: Config reply from server for " + id);

            if (!ConfigBases.ContainsKey(id))
                return;

            if (config == "")
                // server does not know this config yet, so we upload it
                SaveConfig(ConfigBases[id], true);
            else
                JsonConvert.PopulateObject(config, ConfigBases[id]);
        }
    }
}
