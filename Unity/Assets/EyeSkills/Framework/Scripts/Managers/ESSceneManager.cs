/*
    This code belongs to the "EyeSkills Framework". It is designed to assist
    in the development of software for the human/animal visual system.

    Copyright(C) 2018 Dr. Thomas Benjamin Senior, Michael Zöller.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using UnityEngine.SceneManagement;

namespace EyeSkills
{
    public class ESSceneManager : MonoBehaviour
    {
        // scene to be loaded next
        public string sceneToLoad = "";

        // singleton like access pattern
        public static ESSceneManager instance;

        void Awake()
        {
            if (instance == null)
                instance = this;
            else if (instance != this)
                Destroy(gameObject);

            DontDestroyOnLoad(this);
        }

        void OnEnable()
        {
            SceneManager.sceneLoaded += OnSceneLoaded;
        }

        void OnSceneLoaded(Scene scene, LoadSceneMode mode)
        {
            Screen.sleepTimeout = SleepTimeout.NeverSleep;
        }

        void OnDisable()
        {
            SceneManager.sceneLoaded -= OnSceneLoaded;
        }

        private void Update()
        {
            if (UnityEngine.SceneManagement.SceneManager.GetActiveScene().name != "Menu" && (Input.GetKeyDown(KeyCode.Escape) || Input.GetButton("EyeSkills Cancel")))
                SceneManager.LoadScene("Menu");
        }

        public void VRSwitch(bool enableVR)
        {
            if (Application.isEditor)
                return;

            if (!enableVR && XRSettings.enabled)
            {
                Debug.Log("SceneManager: Loading scene directly");
                // disable VR
                XRSettings.enabled = false;
                //Screen.orientation = ScreenOrientation.Portrait;
                string s = SceneManager.GetActiveScene().name;
                SceneManager.LoadScene(s);
            }
            else if (enableVR && !XRSettings.enabled)
            {
                Debug.Log("SceneManager: Loading lobby");
                // enable VR
                sceneToLoad = SceneManager.GetActiveScene().name;
                SceneManager.LoadScene("Lobby");
            }
        }
    }
}
