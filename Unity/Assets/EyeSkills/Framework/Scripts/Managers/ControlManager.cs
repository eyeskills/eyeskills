/*
    This code belongs to the "EyeSkills Framework". It is designed to assist
    in the development of software for the human/animal visual system. 

    Copyright(C) 2018 Dr. Thomas Benjamin Senior, Michael Andreas Zöller.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace EyeSkills
{
    [System.Serializable]
    public class Event
    {
        public Action callback;
        public string description;
        public string name;
        public List<string> keys; //Ben : Why are you serializing all the behaviours?
    };

    public class ControlManager : MonoBehaviour
    {
        public Dictionary<string, Event> behaviours = new Dictionary<string, Event>();

        // singleton like access pattern
        public static ControlManager instance = null;

        void Awake()
        {
            if (instance == null)
                instance = this;
            else if (instance != this)
                Destroy(gameObject);
            DontDestroyOnLoad(this);
        }

        public void RegisterBehaviour(string id, Action callback, string name = "", string description = "")
        {
            Event b = new Event();
            b.description = description;
            b.name = name;
            b.callback = callback;
            b.keys = new List<string>();
            behaviours.Add(id, b);

            string json = "\"{\"id\":\"" + id + "\", \"name\":\"" + name + "\", \"description\":\"" + description + "\"}\"";
            Debug.Log(json);

            //Ben: I assume this is just for testing. It might make sense if there's to be a web interface for establishing mappings, 
            //but that's something else which needs it's own Pub/Sub methods.
            //NetworkManager.instance.Send("register", json);
        }

        public void Register(string id, string key)
        {
            behaviours[id].keys.Add(key);
        }

        /// <summary>
        /// Registers input event types. In order to build a GUI mapping behaviours to inputs, we need to know which inputs there might be.
        /// Here, input devices can register themselves and the events they are able to produce. 
        /// </summary>
        /// <param name="id">Identifier.</param>
        public void RegisterInputEventType(string deviceID, string eventType)
        {
        }

        /// <summary>
        /// Distributes the input event. Any connected input devices should call this method with their updates.
        /// It will distribute that event to any behaviour call-backs connected to that source.
        /// </summary>
        /// <param name="">.</param>
        public void distributeInputEvent(string deviceID, string eventType, string eventData)
        {
        }

        /// <summary>
        /// Adds the mapping from input to behaviour.  
        /// </summary>
        /// <param name="inputEventTypeID">Input event type identifier.</param>
        /// <param name="behaviourID">Behaviour identifier.</param>
        public void addMappingFromBehaviourToInput(string behaviourID, string deviceID, string deviceEventType)
        {
        }

        public void delMappingFromInputToBehaviour(string behaviourID, string deviceID, string deviceEventType)
        {
        }
    }
}