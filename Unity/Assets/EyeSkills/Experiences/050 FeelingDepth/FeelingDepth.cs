/*
    This code belongs to the "EyeSkills Framework". It is designed to assist
    in the development of software for the human/animal visual system. 

    Copyright(C) 2018 Dr. Thomas Benjamin Senior, Michael Zöller.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EyeSkills;

namespace EyeSkills.Experiences
{
    public class FeelingDepth : MonoBehaviour
    {
        public GameObject part1, part2;
        public float horizontalSeparation, previousSeparation;
        Vector3 centerPoint;
        private EyeSkillsInput esInput;
        float rate = 0.2f;
        float step = 0.01f;

        void Start()
        {
            esInput = GetComponent<EyeSkillsInput>();

            centerPoint = part1.transform.position;
        }

        void SeparateParts(GameObject ring1, GameObject ring2)
        {
            // Lets listen for Hold
            if (esInput.GetHeldButtonPress("EyeSkills Right"))
            {
                Debug.Log("increase");
                horizontalSeparation += Time.deltaTime * rate;
            }
            else if (esInput.GetHeldButtonPress("EyeSkills Left"))
            {
                horizontalSeparation -= Time.deltaTime * rate;
            }
            else if (esInput.GetShortButtonPress("EyeSkills Right"))
            {
                horizontalSeparation += step;
            }
            else if (esInput.GetShortButtonPress("EyeSkills Left"))
            {
                horizontalSeparation -= step;
            }

            // TODO: This class will actually just reduce spacing in measured steps... but this test code is almost exactly what we need for the eye straightener
            // TODO: It'll be very similar for trying to keep eye tracking going on an object we move.

            Mathf.Clamp(horizontalSeparation, 0, 40);

            if (!Mathf.Approximately(horizontalSeparation, previousSeparation))
            {
                ring1.transform.position = new Vector3(centerPoint.x + horizontalSeparation, centerPoint.y, centerPoint.z);
                ring2.transform.position = new Vector3(centerPoint.x - horizontalSeparation, centerPoint.y, centerPoint.z);
                previousSeparation = horizontalSeparation;
            }
        }

        void Update()
        {
            SeparateParts(part1, part2);
        }
    }
}