/*
    This code belongs to the "EyeSkills Framework". It is designed to assist
    in the development of software for the human/animal visual system.

    Copyright(C) 2018 Dr. Thomas Benjamin Senior, Michael Zöller.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace EyeSkills.Experiences
{
    public class PhoneCamera : MonoBehaviour
    {
        private bool camAvailable;
        private WebCamTexture backCam;

        public RawImage background;
        public AspectRatioFitter fit;

        void OnEnable()
        {
            SceneManager.sceneUnloaded += OnSceneUnloaded;
        }

        void OnSceneUnloaded(Scene scene)
        {
            if ((background != null) && (background.texture != null)){
                background.texture = null;
            }
            backCam.Stop();
            camAvailable = false;
        }

        void Start()
        {
            Debug.Log("Starting PhoneCamera script");
            WebCamDevice[] devices = WebCamTexture.devices;

            if (devices.Length == 0)
            {
                Debug.Log("No camera detected");
                camAvailable = false;
                return;
            }

            for (int i = 0; i < devices.Length; i++){
                Debug.Log("Found cam " + devices[i].name);
                //if (!devices[i].isFrontFacing){
                //    backCam = new WebCamTexture(devices[i].name,Screen.width, Screen.height);
                //    Debug.Log("Set cam to camera " + i);
                //}

            }
            //0 is main camera with auto focus
            //2 is wide-angle
            Debug.Log("Trying to set Backcamera");
            backCam = new WebCamTexture(devices[0].name, Screen.width, Screen.height);
            Debug.Log("Back camera set");
            if (backCam == null)
            {
                Debug.LogWarning("Could not find backward facing camera.");
                return;
            }
            Debug.Log("Setting texture");
            background.texture = backCam;
            Debug.Log("Background texture set");
            Debug.Log("About to play back camera " + backCam.deviceName + " name: " + backCam.name);
            backCam.Play();
            camAvailable = true;
        }

        void Update()
        {
            if (!camAvailable)
                return;

            float ratio = (float) backCam.width / (float) backCam.height;
            fit.aspectRatio = ratio;

            float scaleY = backCam.videoVerticallyMirrored ? -1f : 1f;
            background.rectTransform.localScale = new Vector3(1f, scaleY, 1f);

            int orient = -backCam.videoRotationAngle;
            background.rectTransform.localEulerAngles = new Vector3(0, 0, orient);
        }
    }
}
