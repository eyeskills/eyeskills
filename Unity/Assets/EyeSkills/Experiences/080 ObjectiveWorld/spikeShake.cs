﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spikeShake : MonoBehaviour {

    int shakeTravel = 2;
    int shakeSensitivity = 3;

	// Use this for initialization
	void Start () {
        //Easy
        Queue<int> q1 = new Queue<int>();
        q1.Enqueue(1);
        q1.Enqueue(1);
        q1.Enqueue(-1);
        q1.Enqueue(-1);
        q1.Enqueue(1);
        q1.Enqueue(1);
        q1.Enqueue(-1);
        q1.Enqueue(-1);
        q1.Enqueue(1);
        q1.Enqueue(1);
        q1.Enqueue(-1);
        q1.Enqueue(-1);
        q1.Enqueue(1); 
        q1.Enqueue(1); 
        q1.Enqueue(1);

        Debug.Log("easy was "+FindDirectionChanges(q1));

        //With Zeros and some falses
        Queue<int> q2 = new Queue<int>();
        q2.Enqueue(1);
        q2.Enqueue(0);
        q2.Enqueue(1);
        q2.Enqueue(-1);
        q2.Enqueue(1);
        q2.Enqueue(-1);
        q2.Enqueue(1);
        q2.Enqueue(-1);
        q2.Enqueue(-1);
        q2.Enqueue(1);
        q2.Enqueue(0);
        q2.Enqueue(-1);
        q2.Enqueue(-1);
        q2.Enqueue(1);
        q2.Enqueue(1);
        q2.Enqueue(1);

        Debug.Log("one match was " + FindDirectionChanges(q2));

    }


    private int FindDirectionChanges(Queue<int> q){

        //As soon as we encounter a boundary, we store the number of consecutive matches we encountered, and start a new counter.
        //Everytime we encounter more than shakeTravel matches, we check if the last set of consecutive matches (which must be the opposite number) were also > shake Travel.
        //If that is the case, we have a shift. This also means that we can move directly from one direction change to another.

        //Nearly, but we need to look ahead, if there is no boundary at the end, we miss a movement, but perhaps that is fine.

        int matchSize = 2;
        int lasti = 2; //out of the ordinary
        int lastGroupCount = 0;
        int groupCount = 0;
        int directionChanges = 0;

        Queue<int> groupQ = new Queue<int>();

        foreach (int i in q){

            if (i!=lasti) { //We have hit a boundary.
                //Are this group count and the last group count both larger than matchSize?
                if ((lastGroupCount>=matchSize) && (groupCount>=matchSize))
                {
                    directionChanges += 1;
                }
                lastGroupCount = groupCount;
                groupCount = 0;
                lasti = i;
            }

            if (i==lasti){
                groupCount += 1;
            }
        }

        return directionChanges;

    }



	 private bool AreThresholdsExceeded(Queue<int> q)
    {
        int lookingFor = 1;
        bool inSecondSection = false;
        int firstSection=0, secondSection=0;
        int shifts = 0;

        //We add up each change in direction... that's not the same as a long sweep left, then a long sweep right
        //We also don't want incredibly rapid switching around the center point.
        //We insist on at least X in one direction, then Y in the other.
        //We do this reasonably clearly with three states...although we could use coroutines etc.
        //We could also pre-scan and group by alternating numbers, then pop those groups off looking for consecutive pairs greater than shakeTravel
        Debug.Log("STARTED LOOPING");
        string set = "";
        foreach (int i in q)
        {
            set += i + " ";
        }
        Debug.Log("QUEUE currently " + set);

        foreach (int i in q)
        {
            //We ignore 0's
            if (i != 0)
            {           
                if (i == lookingFor)
                {
                    Debug.Log("MATCHING " + i);
                    if ((inSecondSection) && (secondSection >= (shakeTravel - 1)))
                    {
                        shifts += 1;
                        lookingFor = -i;
                        inSecondSection = false;
                        firstSection = 0; //Detected a shake. Now look for the opposite shake.
                        Debug.Log("Second Section Completed! Shifts : " + shifts);
                    }
                    else if ((inSecondSection) && (secondSection < (shakeTravel - 1)))
                    {
                        secondSection += 1;
                        Debug.Log("Second Section " + secondSection);
                    }
                    else
                    {
                        firstSection += 1;
                        Debug.Log("First Section " + firstSection);
                    }
                }
                else if (i != lookingFor)
                {

                    Debug.Log("!MATCHING " + i);
                    if (inSecondSection)
                    {
                        Debug.Log("Dropping out of second section. Now looking for " + i);
                        secondSection = 0;
                        lookingFor = i;
                        firstSection = 1;
                    }

                    if ((!inSecondSection) && (firstSection >= shakeTravel))
                    { //If we've had the minimum number of measurements in the same direction...
                        Debug.Log("Found enough of section one, going into section two looking for " + i + "and secondSection = 1");
                        inSecondSection = true;
                        secondSection = 1;
                        lookingFor = i; //Now look for the other thing next
                    }
                    else if (!inSecondSection)
                    {
                        Debug.Log("Starting to look again for " + i);
                        lookingFor = i;  //Try to find some going in the other direction...
                        inSecondSection = false;
                        firstSection = 1;
                    }

                }
            }

        } // We have processed the entire queue.

        Debug.Log("FINISHED LOOPING - shifts set to "+shifts);

        if (shifts >= shakeSensitivity)
        {
            Debug.Log("shakeSensitivity threshold exceeded " + shifts);
            return true;
        }
        //Debug.Log("Thresholds not exceeded ");
        return false;
    }
}
