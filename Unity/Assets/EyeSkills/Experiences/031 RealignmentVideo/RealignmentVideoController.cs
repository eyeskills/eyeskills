/*
    This code belongs to the "EyeSkills Framework". It is designed to assist
    in the development of software for the human/animal visual system. 

    Copyright(C) 2018 Dr. Thomas Benjamin Senior, Michael Zöller.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EyeSkills;

namespace EyeSkills.Experiences
{
    public class RealignmentVideoController : MonoBehaviour
    {
        [System.Serializable]
        public class RealignmentVideoConfig : ConfigBase
        {
            public float leastMisalignmentBeforeFusionLost;
        }

        public RealignmentVideoConfig config;

        // TODO: Why is the binocular suppression ratio not being correctly applied?
        // TODO: Need to also add functionality to alternate between the eyes

        private StereoTargetEyeMask eyeMask;

        private Vector3 misalignmentRotation;
        private float currentAngle;
        private EyeSkillsInput esInput;
        public GameObject blinker1, blinker2;
        private bool straightening = false;

        public float turningRate = 2f;

        EyeSkillsCameraRig cameraRig;

        private void FetchEyeCalibration()
        {
            if (cameraRig.config.rightEyeIsStrabismic)
            {
                eyeMask = StereoTargetEyeMask.Right;
                misalignmentRotation = cameraRig.config.rightEyeMisalignmentAngle;
            }
            else
            {
                //default to left eye
                eyeMask = StereoTargetEyeMask.Left;
                misalignmentRotation = cameraRig.config.leftEyeMisalignmentAngle;
            }
        }

        private void ResetEyeMisalignment()
        {
            Debug.Log("Resetting eye misalignment");
            cameraRig.StraightenLeftEye();
            cameraRig.StraightenRightEye();
            cameraRig.Rotate(eyeMask, misalignmentRotation);
            //misalignedEye.transform.rotation = Quaternion.identity;
            //misalignedEye.transform.Rotate(misalignmentRotation);
        }

        void Start()
        {
            cameraRig = FindObjectOfType<EyeSkillsCameraRig>();

            NetworkManager.instance.RegisterScene("Eye Straightening", "How long can the participant hold fusion as we straighten up their world?");
            NetworkManager.instance.RegisterButton("start", "Start/Re-start straightening", "Start/Re-start straightening the eye from the misaligned position");
            NetworkManager.instance.RegisterButton("stop", "Stop straightening", "Pause the straightening at the point fusion was lost");
            NetworkManager.instance.RegisterFloat("degree", -45f, 45f, 1f, "Misalignment", "Angle between the eyes.");

            esInput = EyeSkillsInput.instance;

            FetchEyeCalibration();
            ResetEyeMisalignment();
            straightening = true; //Lets immediately start straightening.
        }

        void Update()
        {
            // One press to start. Second to stop. Third to (re-)start
            if (esInput.GetShortButtonPress("EyeSkills Confirm") || NetworkManager.instance.GetButton("start") ||
                NetworkManager.instance.GetButton("stop"))
            {
                if (!straightening)
                {
                    ResetEyeMisalignment();
                    straightening = true;
                }
                else if (straightening)
                {
                    straightening = false;

                    // Time to log our new time to the calibration object.
                    // We store the best (least) misalignment the participant achieved as an absolute angle
                    Debug.Log("smallest angle in calibration " + config.leastMisalignmentBeforeFusionLost);
                    Debug.Log("current angle" + currentAngle);

                    if (config.leastMisalignmentBeforeFusionLost > currentAngle)
                    {
                        Debug.Log("Storing least misalignment before fusion lost");
                        config.leastMisalignmentBeforeFusionLost = Mathf.Abs(currentAngle);
                        AudioManager.instance.Say("SavingBestAngle");
                    }
                }
            }

            if (esInput.GetShortButtonPress("EyeSkills Up"))
            {
                AudioManager.instance.Say("BlinkersOn");
                blinker1.SetActive(true);
                blinker2.SetActive(true);
            }
            else if (esInput.GetShortButtonPress("EyeSkills Down"))
            {
                AudioManager.instance.Say("BlinkersOff");
                blinker1.SetActive(false);
                blinker2.SetActive(false);
            }

            if (straightening)
            {
                cameraRig.RotateToStraightenEye(eyeMask, turningRate * Time.deltaTime);

                // The least distance we were away from being straight.
                currentAngle = cameraRig.GetEyeAngle(eyeMask).y; //misalignedEye.transform.rotation.eulerAngles.y;
                if (currentAngle > 180)
                    currentAngle = 360 - currentAngle; // TODO: Need to think this through more thoroughly.

                //currentAngle = Vector3.Angle(misalignedEye.transform.rotation.eulerAngles, nonMisalignedEye.transform.rotation.eulerAngles).y;
                NetworkManager.instance.SetFloat("degree", currentAngle);
            }
        }
    }
}