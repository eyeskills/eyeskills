/*
    This code belongs to the "EyeSkills Framework". It is designed to assist
    in the development of software for the human/animal visual system. 

    Copyright(C) 2018 Dr. Thomas Benjamin Senior, Michael Zöller.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using UnityEngine;
using UnityEngine.Video;
using AVTube;
using SubjectNerd.Utilities;

namespace EyeSkills.Experiences
{
    public class YouTube : MonoBehaviour
    {
        public int resolution = 360;

        [System.Serializable]
        public class YouTubeConfig : ConfigBase
        {
            public int playlistIndex;
            public double videoResumeTime;
            public string[] playlist;
        }

        public YouTubeConfig config;

        private VideoPlayer videoPlayer;
        private float connectionTime;

        void Update()
        {
            if (Input.GetKey(KeyCode.Escape))
                config.videoResumeTime = videoPlayer.time;
        }

        void OnEnable()
        {
            videoPlayer = GetComponent<VideoPlayer>();
            videoPlayer.loopPointReached += loopPointReached;
        }

        void OnDisable()
        {
            videoPlayer.loopPointReached -= loopPointReached;
        }

        void Start()
        {
            if (config.playlistIndex >= config.playlist.Length)
                config.playlistIndex = 0;

            if (config.playlist.Length > 0)
                StartCoroutine(_playVideo(config.playlist[config.playlistIndex]));
            else
            {
                Debug.LogWarning("YouTube: Playlist is empty.");
                VideoFallback();
            }
        }

        void loopPointReached(VideoPlayer vp)
        {
            Debug.Log("YouTube: Loop point reached. Loading next video.");
            config.playlistIndex++;
            if (config.playlistIndex >= config.playlist.Length)
                config.playlistIndex = 0;
            config.videoResumeTime = 0;
            config.Sync();
            connectionTime = Time.time;
            StartCoroutine(_playVideo(config.playlist[config.playlistIndex]));
        }

        IEnumerator _playVideo(string url)
        {
            yield return 0;
            
            // TODO: mono misses some cyphers.
            // TODO: so we ignore YouTube ssl errors when resolving Urls via AVTube
            ServicePointManager.ServerCertificateValidationCallback =
                delegate(object s, X509Certificate certificate,
                    X509Chain chain, SslPolicyErrors sslPolicyErrors)
                {
                    return true;
                };

            IEnumerable<VideoInfo> videoInfos;
            try
            {
                videoInfos = DownloadUrlResolver.GetDownloadUrls(url, true);
            }
            catch
            {
                Debug.LogWarning("YouTube: Network error. Retrying...");
                if( Time.time - connectionTime > 4f)
                    VideoFallback();
                else
                    StartCoroutine(_playVideo(url));
                yield break;
            }

            // TODO: reset certificate validation callback
            ServicePointManager.ServerCertificateValidationCallback = null;

            yield return 0;

            string extension = ".mp4";

#if UNITY_EDITOR_LINUX
            extension = ".webm";
#endif

            foreach (VideoInfo vi in videoInfos)
                if (vi.VideoExtension == extension && vi.Resolution == resolution)
                {
                    Debug.Log("Playing " + vi);

                    videoPlayer.source = VideoSource.VideoClip;
                    // BUG: unity bug workaround: video url must end in extension
                    // BUG: we just add it as a query string parameter that the server ignores 
                    videoPlayer.url = vi.DownloadUrl + "&" + extension;
                    videoPlayer.Play();
                    videoPlayer.time = config.videoResumeTime;

                    yield break;
                }

            Debug.LogWarning("YouTube: Could not find a valid download matching request resolution and extension.");
        }

        void VideoFallback()
        {
            videoPlayer.source = VideoSource.VideoClip;
            videoPlayer.Play();
        }
    }
    
}